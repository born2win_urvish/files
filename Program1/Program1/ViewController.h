//
//  ViewController.h
//  Program1
//
//  Created by MAC on 12/24/15.
//  Copyright (c) 2015 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>
{
    NSMutableString *input;
    NSString *test;
    int start,end,divisior;
    NSMutableArray *checkData;
}

@property (weak, nonatomic) IBOutlet UITextField *txtStartRange;

@property (weak, nonatomic) IBOutlet UITextField *txtEndRange;

@property (weak, nonatomic) IBOutlet UITextField *txtDivisor;

- (IBAction)Calculate:(id)sender;


@property (weak, nonatomic) IBOutlet UITextView *textview1;


- (IBAction)cal7:(id)sender;

- (IBAction)data:(id)sender;

@end

