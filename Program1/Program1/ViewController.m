//
//  ViewController.m
//  Program1
//
//  Created by MAC on 12/24/15.
//  Copyright (c) 2015 MAC. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
            

@end

@implementation ViewController
@synthesize txtStartRange,txtEndRange,txtDivisor,textview1;
            
- (void)viewDidLoad {
    [super viewDidLoad];
    
    input=[[NSMutableString alloc]initWithString:@"Simple Reversable:"];

    
    NSLog(@"Program Starts");
    for (int i=300; i>0; i--) {
        if (i%7==0) {
            NSLog(@"%d",i);
            test=[NSString stringWithFormat:@"%d",i];
            [input appendString:@" "];
            [input appendString:test];
        }
    }
    NSLog(@"%@",[input description]);
    textview1.text=input;
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


- (IBAction)Calculate:(id)sender {
    textview1.text=@" ";
    
    input=[[NSMutableString alloc]initWithString:@"User Input Reversable:"];
    
    start=[txtStartRange.text integerValue];
    end=[txtEndRange.text integerValue];
    divisior=[txtDivisor.text integerValue];
    
    if (start>0 && end>0 && divisior>0) {
        if (end>start) {
            for (int i=end; i>start; i--) {
                if (i%divisior==0) {
                    test=[NSString stringWithFormat:@"%d",i];
                    [input appendString:@" "];
                    [input appendString:test];
                }
            }
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Pay Attention" message:@"Invalid data input" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Pay Attention" message:@"Invalid data input" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    NSLog(@"%@",[input description]);
    textview1.text=input;
    
    txtDivisor.text=@"";
    txtEndRange.text=@"";
    txtStartRange.text=@"";
    
    [self.view endEditing:YES];
    
    
    
}
- (IBAction)cal7:(id)sender {
    
    [self viewDidLoad];
    
}

- (IBAction)data:(id)sender {
    
    NSString *post=[NSString stringWithFormat:@"http://localhost/temp/index.php?select=select * from tmp_data"];
    
    NSURL *url=[NSURL URLWithString:[post stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSData *dt=[[NSData alloc]initWithContentsOfURL:url];
    NSError *err;
    
    checkData=[NSJSONSerialization JSONObjectWithData:dt options:kNilOptions error:&err];
    
    //NSLog(@"%@",[checkData description]);
    
    
    NSString *strName;
    NSString *strCity;
    NSString *strEmail;
 
    NSMutableArray *arrDetail=[[NSMutableArray alloc]init];
    
    for (NSDictionary *dic in checkData) {
        
       // NSLog(@"%@",[dic description]);
      
        strName=[dic objectForKey:@"name"];
        strCity=[dic objectForKey:@"city"];
        strEmail=[dic objectForKey:@"email"];
        
        
      
        [arrDetail addObject:strName];
        [arrDetail addObject:strCity];
        [arrDetail addObject:strEmail];
        [arrDetail addObject:@"\n"];
        
    }
    
    NSLog(@"%@",[arrDetail description]);
    //textview1.text=[checkData description];
    
    NSString *printStr=[arrDetail componentsJoinedByString:@"\n"];
    
    textview1.text=printStr;
    
}
@end
